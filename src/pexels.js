export const loadImages = () => {
  const page = parseInt(Math.random() * 100, 10);
  return fetch(`https://api.pexels.com/v1/search?query=nature+landscape&per_page=5&page=${page}`,
    { headers: { Authorization: '563492ad6f9170000100000120ba6b83629e4659b42a5279af677960' } })
    .then(res => res.json())
    .then(result => result.photos);
};
